package com.my.filereader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	
	//public static final String EXCELFILELOCATION = System.getProperty("user.dir") + "//src//test//resources//TestDataSheet.xlsx";
	public static final String EXCELFILELOCATION = "./TestDataSheet.xlsx";
	public static FileInputStream fis;
	private static XSSFWorkbook workbook;
	private static XSSFSheet sheet;
	private static XSSFRow row;
	
	
	public static void loadExcel() throws IOException {
		System.out.println("Loading Excel Data....");
		
		File file = new File(EXCELFILELOCATION);
		fis = new FileInputStream(file);
		workbook = new XSSFWorkbook(fis);
		sheet = workbook.getSheet("sheet1");
	}
	
	
	public static Map<String, Map<String, String>> getDataMap() throws Exception{
		if(sheet==null) {
			loadExcel();
		}
		
		Map<String, Map<String, String>> superMap = new HashMap<String, Map<String, String>>();
		Map<String, String> myMap = new HashMap<String, String>();
			
		for(int i=1; i < sheet.getLastRowNum() + 1; i++) {
			row = sheet.getRow(i);
			String keyCell = row.getCell(0).getStringCellValue();			
			
			int colNum = row.getLastCellNum();
			for(int j=1; j<colNum; j++) {
				String valueCell = row.getCell(j).getStringCellValue();
				myMap.put(keyCell, valueCell);
			}
			superMap.put("masterData", myMap);
		}
				
		return superMap;		
	}
	
	
	public static String getValue(String key) throws Exception {
		Map<String, String> myVal = getDataMap().get("masterData");
		return myVal.get(key);
	}
	
}








